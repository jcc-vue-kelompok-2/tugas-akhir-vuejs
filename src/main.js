import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import axios from './plugins/axios'
import store from './store'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.config.productionTip = false
Vue.prototype.$apiUrl = 'https://demo-api-vue.sanbercloud.com/api/v2'
Vue.prototype.$imageUrl = 'https://demo-api-vue.sanbercloud.com'

Vue.mixin({
  methods: {
    validateForm(condition, message) {
      if (condition) {
        this.$store.commit('alert/set', {
          status: true,
          color: 'error',
          text: message
        })
        return false
      }
      return true
    }
  }
})

new Vue({
  router,
  vuetify,
  axios,
  store,
  render: h => h(App)
}).$mount('#app')
