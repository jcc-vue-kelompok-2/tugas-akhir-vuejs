import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/blogs',
    name: 'Blogs',
    component: () => import('../views/BlogsView.vue')
  },
  {
    path: '/blog/create',
    name: 'Buat Blog',
    component: () => import('../views/BlogFormView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/blog/edit/:id',
    name: 'Edit Blog',
    component: () => import('../views/BlogFormView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    component: () => import('../views/BlogView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let token = store.state.auth.token  
    if (token.length === 0) {
      next('/')
      store.commit('alert/set', {
        status: true,
        color: 'error',
        text: 'Anda harus login terlebih dahulu.',
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
