# Tugas Akhir VueJS

## Kelompok 2

- [Muhammad Esa Yusriana](https://gitlab.com/mesayusriana12)
- [Sinta Nurleta](https://gitlab.com/Sinta-Nurleta)
- [Fakhri Al Fatah](https://gitlab.com/fakhrialfth)
- Nugi Nugraha

## Instalasi

```
git clone https://gitlab.com/jcc-vue-kelompok-2/tugas-akhir-vuejs.git

cd tugas-akhir-vuejs

npm install

npm run serve
```

## Library Yang Digunakan

- [Vue Router](https://router.vuejs.org/)
- [Vue-Axios](https://www.npmjs.com/package/vue-axios)
- [Vuetify](https://vuetifyjs.com/en/)
- [Vuex](https://vuex.vuejs.org/)
- [Vuex-Persistedstate](https://www.npmjs.com/package/vuex-persistedstate)

## Demo Aplikasi

- Link Website : "https://finalproject-kelompok2-jcc.netlify.app/"
- Link Video Demo : "https://youtu.be/yg50ztO4c5M"
